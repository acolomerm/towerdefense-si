using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "ScriptableCards", menuName = "Cards")]
public class CardsScriptable : ScriptableObject
{

    // Variables
    [Header("Cards Variables")]

    public int id;

    [Multiline(3)]
    public string Name;

    public Sprite[] img;

    public GameObject GameObject;

    public int hpMax;

    public int currentHp;

    public int damage;

    public float atackSpeed;

    public float moveSpeed;

    [Range(1, 13)]
    public int range;

    //public ArrayList<Abilities> cardAbilities;

    public float abilitieCost;

    public int jewelCost;

    public int cardLevel;

    public int price;

    public bool hero;

    public bool unlock;

    public int levelRequest;

    public int levelInGame;

    public bool enemy;


    public void setCurrentHp()
    {

        this.currentHp = hpMax;

    }

    public void cardDie()
    {

        if(currentHp <= 0)
        {

            Debug.Log("Muerto");

        }

    }

}
