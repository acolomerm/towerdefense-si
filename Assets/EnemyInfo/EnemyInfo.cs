using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy Info", menuName = "Scriptables/Enemy Info")]
public class EnemyInfo : ScriptableObject
{
    public Color color;
    public Vector3 scale;
    public float angularSpeed;
}
