using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEditor;
using UnityEditor.EditorTools;
using UnityEngine;
using UnityEngine.Tilemaps;


[RequireComponent(typeof(Pathfinding))]
public class InstanciateMobs : MonoBehaviour
{
    // Tilemap
    [SerializeField]
    private Tilemap m_Tilemap;
    //GameObject
    [SerializeField]
    private GameObject generalMob;

    private Vector3Int originNode = new Vector3Int(-5, 1, 0);

    public GameObject[] cardInvokeController;

    private void Awake()
    {

    }

    private void Start()
    {

        cardInvokeController[0].GetComponent<cardInvokeController>().card += AssignScriptableObject;
        cardInvokeController[1].GetComponent<cardInvokeController>().card += AssignScriptableObject;
        cardInvokeController[2].GetComponent<cardInvokeController>().card += AssignScriptableObject;
        
            CardsScriptable cardMob = GameManager.Instance.myTeam[0];
            cardMob.enemy = false;
            GameObject newMob = Instantiate(generalMob);
            newMob.GetComponent<mobController>().statSO = cardMob;
            newMob.transform.position = m_Tilemap.GetCellCenterWorld(new Vector3Int(this.originNode.x, this.originNode.y, 0));
            //newMob.GetComponent<mobController>().origin = originNode;
            newMob.GetComponent<mobController>().origin = new Vector3Int(this.originNode.x, this.originNode.y, 0);
            GameManager.Instance.aliesPosition.Add(new Vector3Int(this.originNode.x, this.originNode.y, 0));
        
    }

    private void Update()
    {
        //if (!GameManager.Instance.runBattle)
        //{

        //if (Input.GetMouseButtonDown(0))
        //{
        //    lookingNode = m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        //    for (int i = 0; i < ocupedNodes.Count; i++)
        //    {
        //        if (ocupedNodes[i] == lookingNode)
        //        {
        //            RemoveNode(i);
        //            previousNode = lookingNode;
        //            m_EditMode = true;

        //        }
        //    }
        //}

        //if(Input.GetMouseButtonDown(0))
        //{
        //    coordenadesTilemap = m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));

        //    if (m_Pathfinding.IsWalkableTile(coordenadesTilemap) && ocupedNodes.Contains(coordenadesTilemap) && coordenadesTilemap.x < 0)
        //    {
        //        previousNode = coordenadesTilemap;
        //        m_EditMode = true;
        //        m_EditModeObject = Instantiate(generalMob);
        //        Destroy(m_EditModeObject.GetComponent<Rigidbody2D>());
        //    }
        //    else
        //    {
        //        m_EditMode = false;
        //        Destroy(m_EditModeObject);
        //    }

        //}

        //if (Input.GetMouseButtonUp(0))
        //{
        //    if (m_EditMode)
        //    {
        //        coordenadesTilemap = m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        //        if (!m_Pathfinding.IsWalkableTile(coordenadesTilemap) && !ocupedNodes.Contains(previousNode) || 
        //            ocupedNodes.Contains(coordenadesTilemap) && m_Pathfinding.IsWalkableTile(previousNode) ||
        //            m_Pathfinding.IsWalkableTile(coordenadesTilemap) && coordenadesTilemap.x >= 0)
        //        {
        //            m_EditMode = false;
        //            Destroy(m_EditModeObject);
        //            GameObject newMob = Instantiate(generalMob);
        //            newMob.transform.position=m_Tilemap.GetCellCenterWorld(previousNode);
        //            newMob.GetComponent<mobController>().origin = previousNode;                        
        //            AddNode(previousNode);
        //            GameManager.Instance.aliesPosition.Add(previousNode);

        //        }
        //        else if (!ocupedNodes.Contains(coordenadesTilemap) && m_Pathfinding.IsWalkableTile(coordenadesTilemap) && coordenadesTilemap.x < 0)
        //        {
        //            m_EditMode = false;
        //            Destroy(m_EditModeObject);
        //            GameObject newMob = Instantiate(generalMob);
        //            newMob.transform.position = m_Tilemap.GetCellCenterWorld(coordenadesTilemap);
        //            newMob.GetComponent<mobController>().origin = coordenadesTilemap;
        //            AddNode(coordenadesTilemap);
        //            GameManager.Instance.aliesPosition.Add(coordenadesTilemap);
        //        }
        //        else
        //        {
        //            m_EditMode = false;
        //            Destroy(m_EditModeObject);
        //        }
        //    }
        //}

        //if (m_EditMode)
        //{
        //    Vector3 pointerPosition = m_Tilemap.GetCellCenterWorld(m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)));
        //    pointerPosition.z = 0;
        //    m_EditModeObject.transform.position = pointerPosition;

        //}
        //}
    }

    public void EnemyMobs()
    {
        
            
                Debug.Log("lo intento, vale?");
                int randomX = UnityEngine.Random.Range(0, 6);
                int randomY = UnityEngine.Random.Range(-2, 3);
                Vector3Int randomVector = new Vector3Int(randomX, randomY, 0);
                if (!GameManager.Instance.enemyPosition.Contains(randomVector))
                {
                    Debug.Log("Esta estrecho");
                    CardsScriptable cardMob = GameManager.Instance.allMobs[0];
                    cardMob.enemy = true;
                    GameObject newMob = Instantiate(generalMob);
                    newMob.GetComponent<mobController>().statSO = cardMob;
                    newMob.transform.position = m_Tilemap.GetCellCenterWorld(randomVector);
                    //newMob.GetComponent<mobController>().origin = originNode;
                    newMob.GetComponent<mobController>().origin = randomVector;
                    GameManager.Instance.enemyPosition.Add(randomVector);
                    
                }
            
        
        for (int i = 0; i < 4; i++)
        {
            while (true)
            {
                Debug.Log("lo intento, vale?");
                randomX = UnityEngine.Random.Range(0, 6);
                randomY = UnityEngine.Random.Range(-2, 3);
                randomVector = new Vector3Int(randomX, randomY, 0);
                if (!GameManager.Instance.enemyPosition.Contains(randomVector))
                {
                    int randomNumber = UnityEngine.Random.Range(1, GameManager.Instance.allMobs.Count);
                    Debug.Log("Esta estrecho");
                    CardsScriptable cardMob = GameManager.Instance.allMobs[randomNumber];
                    cardMob.enemy = true;
                    GameObject newMob = Instantiate(generalMob);
                    newMob.GetComponent<mobController>().statSO = cardMob;
                    newMob.transform.position = m_Tilemap.GetCellCenterWorld(randomVector);
                    //newMob.GetComponent<mobController>().origin = originNode;
                    newMob.GetComponent<mobController>().origin = randomVector;
                    GameManager.Instance.enemyPosition.Add(randomVector);
                    break;
                }
            }
        }

    }
    private void AssignScriptableObject(int id)
    {

        for (int i = 0; i < GameManager.Instance.myTeam.Length; i++)
        {
            if (GameManager.Instance.myTeam[i].id == id)
            {
                while (true)
                {
                    int randomX = UnityEngine.Random.Range(-5, -1);
                    int randomY = UnityEngine.Random.Range(-2, 3);
                    Vector3Int randomVector = new Vector3Int(randomX, randomY, 0);
                    if (!GameManager.Instance.aliesPosition.Contains(randomVector))
                    {
                        CardsScriptable cardMob = GameManager.Instance.myTeam[i];
                        cardMob.enemy = false;
                        GameObject newMob = Instantiate(generalMob);
                        newMob.GetComponent<mobController>().statSO = cardMob;
                        newMob.transform.position = m_Tilemap.GetCellCenterWorld(randomVector);
                        //newMob.GetComponent<mobController>().origin = originNode;
                        newMob.GetComponent<mobController>().origin = randomVector;
                        GameManager.Instance.aliesPosition.Add(randomVector);
                        break;
                    }
                }
            }
        }

    }

}
