using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardDisplay : MonoBehaviour
{

    public CardsScriptable Card;

    public TMP_Text nameText;

    public TMP_Text JewelText;
    public TMP_Text AbilitieCostText;

    public Image img;

    public TMP_Text damageText;
    public TMP_Text healthText;
    public TMP_Text rangeText;
    public TMP_Text atackSpeedText;
    public TMP_Text moveSpeedText;


    // Start is called before the first frame update
    void Start()
    {

        Card.setCurrentHp();

        nameText.text = "Name: "+Card.Name;
        JewelText.text = "Jewel Cost: "+Card.jewelCost;
        AbilitieCostText.text = "Abilitie Cost: "+Card.abilitieCost;
        img.sprite = Card.img[0];
        damageText.text = "Damage: "+Card.damage;
        healthText.text = "Healt: "+Card.hpMax;
        rangeText.text = "Range: " + Card.range;
        atackSpeedText.text = "Atack Speed: "+Card.atackSpeed;
        moveSpeedText.text = "Move Speed: "+Card.moveSpeed;

    }

    private void Update()
    {
        Card.cardDie();
    }


}
                      