using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;
    public int level = 1;
    public bool runBattle;
    public int jewel;
    public bool zonaVenta;
    public List<Vector3Int> enemyPosition = new List<Vector3Int>();
    public List<Vector3Int> aliesPosition = new List<Vector3Int>();
    public GameObject player;
    public Tilemap sceneTileMap;
    public List<CardsScriptable> allMobs = new List<CardsScriptable>();
    public CardsScriptable[] myTeam = new CardsScriptable[6];   

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
        runBattle = false;
    }

    // Start is called before the first frame update
    

}
