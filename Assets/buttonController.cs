using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class buttonController : MonoBehaviour
{
    public GameObject cards;
    public GameObject refresh;
    public GameObject mana;
    public Tilemap tilemap;

    public GameEvent buttonPress;
    public void ClickPlay()
    {
        this.gameObject.SetActive(false);
        cards.gameObject.SetActive(false);
        refresh.gameObject.SetActive(false);
        mana.gameObject.SetActive(false);
        tilemap.gameObject.SetActive(false);
        buttonPress.Raise();
        //Activar variable creada en singleton de battleRun
        GameManager.Instance.runBattle = true;
    }

}
